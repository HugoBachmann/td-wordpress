<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tdwp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!jYe*DDKioM(} K|p7{W- [ fIwyKMacvz| Pvc*(eHIE>zQQ<r_nZmCR8AESl@(' );
define( 'SECURE_AUTH_KEY',  'cpp|2.A_iPf`Uap?!HqW;r1G)6[7#q:?`.O|Vg;c&4Hc|B2JJ/;8QOF/A6D~Y`UJ' );
define( 'LOGGED_IN_KEY',    '`HfyD1Z1^GP8xaugL>`%LxIti5cM>=2%A>77rEJ1AKJL{pVJHe8Y,||aHEROgnkH' );
define( 'NONCE_KEY',        '%e:fi<rp*;]Vqmn;Uw,T4|yZyg$u*<~n&g`Org]sqDJ8oam].?&_(pSd>].w:q$M' );
define( 'AUTH_SALT',        ' SJx:96f@ KF)es.t/p)*%{wv5n>vv|YZ4;Gkcn&sN1*vV+L@j/. 5f_y8]w1 /{' );
define( 'SECURE_AUTH_SALT', 'fCG0$|G)OO7EL7YMkZuyJY/KYWk,>D_YL7a/Z;r|A1wBu5N{!S8`[1.}*G/M6=2s' );
define( 'LOGGED_IN_SALT',   'jFBtHhT~kCy^/3&|T)Q[vXx<@wo-|v-~Nx@q+4MNMrwh>GazS/yOsdvm|s=&^&Cj' );
define( 'NONCE_SALT',       ':Q:j1A`Nf2@CLobr;S3F-JLp!D%df{A{Oe_>aDc*X.Fv.LPY-Zq,*]</_A+zghuB' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
