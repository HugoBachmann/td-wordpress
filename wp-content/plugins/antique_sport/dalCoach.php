<?php


class dalCoach
{
    private $myWpdb;

    public function __construct()
    {
        global $wpdb;
        $this->myWpdb = $wpdb;
    }

    // add la table coach

    public static function installC()
    {
        global $wpdb;
        $query = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}coach ( " .
            " id INT AUTO_INCREMENT PRIMARY KEY, ".
            " nom VARCHAR(200) NOT NULL, ".
            " prenom VARCHAR(200) NOT NULL, ".
            " discipline VARCHAR(200) NOT NULL, ".
            " description VARCHAR(200) NOT NULL".
            ") ";
        $wpdb->query($query);
    }

    // supprime la table coach

    public static  function uninstallC()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}coach");
    }


    public function findALlC()
    {
        $res = $this->myWpdb->get_results("SELECT * FROM {$this->myWpdb->prefix}coach;", ARRAY_A);
        return $res;
    }

    public function saveCoach()
    {
        $test = true;
        $array_var = ['description', 'nom', 'prenom','discipline'];
        $insert = [];
        foreach ($array_var as $key){
            if( !array_key_exists($key, $_POST)
                || empty($_POST[$key])
                ){
                $test = false;
                break;
            } else {
                $insert[$key] = $_POST[$key];
            }
        }

        if($test){    
            $this->myWpdb->insert("{$this->myWpdb->prefix}coach", $insert );
            
        } else {
            echo "Il y a un problème avec les données proposées ";
        }
    }

    public function deleteByIdC($id)
    {
        if(!is_array($id)){
            $id = [$id];
        }

        $this->myWpdb->query("DELETE FROM {$this->myWpdb->prefix}coach WHERE id in (" . implode(',', $id) . ");");

    }
}
