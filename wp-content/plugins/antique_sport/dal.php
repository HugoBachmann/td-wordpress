<?php


class dal
{
    private $myWpdb;

    public function __construct()
    {
        global $wpdb;
        $this->myWpdb = $wpdb;
    }

    /**
     * création de la table à l'activation du plugin
     */
    public static function install()
    {
        global $wpdb;
        $query = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}membres ( " .
            " id INT AUTO_INCREMENT PRIMARY KEY, ".
            " nom VARCHAR(200) NOT NULL, ".
            " prenom VARCHAR(200) NOT NULL, ".
            " mdp VARCHAR(200) NOT NULL, ".
            " email VARCHAR(255) NOT NULL ".
            ") ";
        $wpdb->query($query);
    }

    /**
     * suppresion de la table à la suppresion du plugin
     */
    public static  function uninstall()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}membres");
    }


    public function findALl()
    {
//        global $wpdb;
        $res = $this->myWpdb->get_results("SELECT * FROM {$this->myWpdb->prefix}membres;", ARRAY_A);
        return $res;
    }

    public function saveMember()
    {
        $test = true; // on init un flag à true
        $array_var = ['email', 'nom', 'prenom', 'mdp'];
        $insert = [];
        foreach ($array_var as $key){
            if( !array_key_exists($key, $_POST) // si le post n'existe pas
                || empty($_POST[$key]) // si le post est vide
                ){
                $test = false;
                break;
            } else {
                $insert[$key] = $_POST[$key];
            }
        }

        if($test){
//            global $wpdb;
            $row = $this->myWpdb->get_row("SELECT * FROM {$this->myWpdb->prefix}membres WHERE email='" .
                $insert['email'] . "';");
            if( is_null($row) ){
                $this->myWpdb->insert("{$this->myWpdb->prefix}membres", $insert );
            } else {
                echo "Il y a un déjà un membre avec cet email ";
            }
        } else {
            echo "Il y a un problème avec les données proposées ";
        }
    }

    public function deleleById($id)
    {
        if(!is_array($id)){
            $id = [$id];
        }

        $this->myWpdb->query("DELETE FROM {$this->myWpdb->prefix}membres WHERE id in (" . implode(',', $id) . ");");

    }
}
