<?php
class dalAqua
{
    private $myWpdb;

    public function __construct()
    {
        global $wpdb;
        $this->myWpdb = $wpdb;
    }


    public static function installAqua()
    {
        global $wpdb;
        $query = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}aqua ( " .
            " id INT AUTO_INCREMENT PRIMARY KEY, ".
            " nom VARCHAR(200) NOT NULL, ".
            " prenom VARCHAR(200) NOT NULL".
            ") ";
        $wpdb->query($query);
    }

    public static  function uninstallAqua()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}aqua");
    }


    public function findALlAqua()
    {
        $res = $this->myWpdb->get_results("SELECT * FROM {$this->myWpdb->prefix}aqua;", ARRAY_A);
        return $res;
    }

    public function saveAqua()
    {  
        $test = true;
        $array_var = ['nom', 'prenom'];
        $insert = [];
        foreach ($array_var as $key){
            if( !array_key_exists($key, $_POST)
                || empty($_POST[$key])
                ){
                $test = false;
                break;
            } else {
                $insert[$key] = $_POST[$key];
            }
        }

        if($test){    
            $this->myWpdb->insert("{$this->myWpdb->prefix}aqua", $insert );
            
        } else {
            echo "Il y a un problème avec les données proposées ";
        }
    }

    public function deleteByIdAqua($id)
    {
        if(!is_array($id)){
            $id = [$id];
        }

        $this->myWpdb->query("DELETE FROM {$this->myWpdb->prefix}aqua WHERE id in (" . implode(',', $id) . ");");

    }
}
