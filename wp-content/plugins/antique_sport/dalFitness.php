<?php
class dalFitness
{
    private $myWpdb;

    public function __construct()
    {
        global $wpdb;
        $this->myWpdb = $wpdb;
    }


    public static function installFitness()
    {
        global $wpdb;
        $query = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}fitness ( " .
            " id INT AUTO_INCREMENT PRIMARY KEY, ".
            " nom VARCHAR(200) NOT NULL, ".
            " prenom VARCHAR(200) NOT NULL".
            ") ";
        $wpdb->query($query);
    }

    public static  function uninstallFitness()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}fitness");
    }


    public function findALlFitness()
    {
        $res = $this->myWpdb->get_results("SELECT * FROM {$this->myWpdb->prefix}fitness;", ARRAY_A);
        return $res;
    }

    public function saveFitness()
    {
        $test = true;
        $array_var = ['nom', 'prenom'];
        $insert = [];
        foreach ($array_var as $key){
            if( !array_key_exists($key, $_POST)
                || empty($_POST[$key])
                ){
                $test = false;
                break;
            } else {
                $insert[$key] = $_POST[$key];
            }
        }

        if($test){    
            $this->myWpdb->insert("{$this->myWpdb->prefix}fitness", $insert );
            
        } else {
            echo "Il y a un problème avec les données proposées ";
        }
    }

    public function deleteByIdFitness($id)
    {
        if(!is_array($id)){
            $id = [$id];
        }

        $this->myWpdb->query("DELETE FROM {$this->myWpdb->prefix}fitness WHERE id in (" . implode(',', $id) . ");");

    }
}
