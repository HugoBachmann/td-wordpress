<?php
class dalMachine
{
    private $myWpdb;

    public function __construct()
    {
        global $wpdb;
        $this->myWpdb = $wpdb;
    }


    public static function installMachine()
    {
        global $wpdb;
        $query = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}machine ( " .
            " id INT AUTO_INCREMENT PRIMARY KEY, ".
            " nom VARCHAR(200) NOT NULL, ".
            " prenom VARCHAR(200) NOT NULL,".
            " dateM DATE NOT NULL,".
            " heure VARCHAR(200) NOT NULL".
            ") ";
        $wpdb->query($query);
    }

    public static  function uninstallMachine()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}machine");
    }


    public function findALlMachine()
    {
        $res = $this->myWpdb->get_results("SELECT * FROM {$this->myWpdb->prefix}machine;", ARRAY_A);
        return $res;
    }

    public function saveMachine()
    {
        $test = true;
        $array_var = ['nom', 'prenom', 'dateM', 'heure'];
        $insert = [];
        foreach ($array_var as $key){
            if( !array_key_exists($key, $_POST)
                || empty($_POST[$key])
                ){
                $test = false;
                break;
            } else {
                $insert[$key] = $_POST[$key];
            }
        }

        if($test){    
            $this->myWpdb->insert("{$this->myWpdb->prefix}machine", $insert );
            
        } else {
            echo "Il y a un problème avec les données proposées ";
        }
    }

    public function deleteByIdMachine($id)
    {
        if(!is_array($id)){
            $id = [$id];
        }

        $this->myWpdb->query("DELETE FROM {$this->myWpdb->prefix}machine WHERE id in (" . implode(',', $id) . ");");

    }
}
