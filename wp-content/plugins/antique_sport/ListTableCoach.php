<?php
if( ! class_exists('WP_List_Table')){
    require_once ABSPATH.'wp-admin/includes/class-wp-list-table.php';
}
require_once plugin_dir_path( __FILE__ ) . 'dalCoach.php';

class ListTableCoach extends WP_List_Table
{
    private  $dalCoach;

    public function __construct()
    {
        parent::__construct([
           'singular' => __('coach', 'cfa'),
           'plural' => __('coach', 'cfa'),
        ]);
        $this->dalCoach = new dalCoach();
    }

    public function prepare_items()
    {
       $columns = $this->get_columns();
       $hiddens = $this->get_hidden_columns();
       $sortables = $this->get_sortable_columns();
       $this->process_bulk_action();
       $perPage = $this->get_items_per_page('nom_per_page', 20);
       $currentPage = $this->get_pagenum();
       $data = $this->dalCoach->findALlC();
       $totalPage = count($data);
//       var_dump($data);
       usort($data, array(&$this, 'usort_reorder'));
//       var_dump($data);
       $paginateData = array_slice($data, (($currentPage - 1) * $perPage ), $perPage);

       $this->set_pagination_args([
           'total_items' => $totalPage,
           'per_page' => $perPage,
       ]);

       $this->_column_headers = array($columns, $hiddens, $sortables);
       $this->items = $paginateData;

    }

    function usort_reorder($a, $b)
    {
        $orderBy = ( !empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'nom';

        $sort = ( !empty( $_GET['order'] ) ) ? mb_strtoupper($_GET['order']) : 'ASC';

        $result = strcmp( $a[$orderBy], $b[$orderBy] );

        return ( $sort === 'ASC' ) ? $result : -$result;
    }

    /**
     * définition des colonnes
     * @return array|string[]
     */
    public function get_columns()
    {
        $columns = [
            'cb' => '<input type="checkbox"/>',
            'nom' => 'nom',
            'prenom' => 'prenom',
            'description' => 'description',
            'discipline' => 'discipline'
        ];
        return $columns;
    }

    /**
     * défifintion des colonnes à trier
     * @return array|array[]
     */
    public function get_sortable_columns()
    {
        return [
            'nom' => array('nom', false),
            'prenom' => array('prenom', false),
            'description' => array('description', false),
            'discipline' => array('discipline', false),
            ];
    }

    public function column_default($item, $column_name)
    {
        switch ( $column_name ){
            case 'id':
            case 'nom':
            case 'prenom':
            case 'description':
            case 'discipline':
                return $item[$column_name];
            default:
                return print_r( $item, true);
        }
    }

    /**
     * définitionn des selecteur d'action
     * @return array|string[]
     */
    public function get_bulk_actions()
    {
        return [
            'delete' => 'Supprimer',
        ];
    }

    public function column_cb($item)
    {
        return sprintf("<input type='checkbox' name='id[]' value='%s' />", $item['id']);
    }

    public function process_bulk_action()
    {

        if('delete' == $this->current_action()){
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : [];
            if(!empty($ids))
                $this->dalCoach->deleteByIdC($ids);
        }
    }

    public function column_nom($item)
    {
        $actions = [ // définition des action indiduelle rapide
            'delete' => sprintf('<a href="?page=%s&action=%s&id=%s">supprimer</a>', $_REQUEST['page'], 'delete',$item['id']),
        ];
        return sprintf('%1$s %2$s', $item['nom'], $this->row_actions($actions));
    }

    /**
     * définition des colonnes vides
     * @return array
     */
    private function get_hidden_columns()
    {
        return [];
    }
}
