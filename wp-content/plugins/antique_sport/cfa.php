<?php
/*
 * Plugin Name: Antique Sport
 * Description: Plugin du site antique 
 * Author: Hugo Bachmann
 * version: 1.1.0
 */

require_once plugin_dir_path(__FILE__) . "/widget/register_widget.php";
require_once plugin_dir_path(__FILE__) . "/widget/registerAqua_widget.php";
require_once plugin_dir_path(__FILE__) . "/widget/registerFitness_widget.php";
require_once plugin_dir_path(__FILE__) . "/widget/registerMachine_widget.php";
require_once plugin_dir_path(__FILE__) . "/dal.php";
require_once plugin_dir_path(__FILE__) . "/dalCoach.php";
require_once plugin_dir_path(__FILE__) . "/dalAqua.php";
require_once plugin_dir_path(__FILE__) . "/dalFitness.php";
require_once plugin_dir_path(__FILE__) . "/dalMachine.php";
require_once plugin_dir_path(__FILE__) . "/ListTableMember.php";
require_once plugin_dir_path(__FILE__) . "/ListTableCoach.php";
require_once plugin_dir_path(__FILE__) . "/ListTableAqua.php";
require_once plugin_dir_path(__FILE__) . "/ListTableFitness.php";
require_once plugin_dir_path(__FILE__) . "/ListTableMachine.php";

class cfa
{
    /**
     * @var slug du plugin
     */
    protected $plugin_slug;

    /**
     * @var
     */
    private static $instance;

    /**
     * @var tableau des templates
     */
    protected $templates;


    /**
     * cfa constructor.
     */
    public function __construct()
    {
        new dal();

        // init de la table en BDD
        register_activation_hook(__FILE__, array('dal', 'install'));
        register_activation_hook(__FILE__, array('dalCoach', 'installC'));
        register_activation_hook(__FILE__, array('dalAqua', 'installAqua'));
        register_activation_hook(__FILE__, array('dalFitness', 'installFitness'));
        register_activation_hook(__FILE__, array('dalMachine', 'installMachine'));


        // suppression du plugin
        register_uninstall_hook(__FILE__, array('dal', 'uninstall'));
        register_uninstall_hook(__FILE__, array('dalCoach', 'uninstallC'));
        register_uninstall_hook(__FILE__, array('dalAqua', 'uninstallAqua'));
        register_uninstall_hook(__FILE__, array('dalFitness', 'uninstallFitness'));
        register_uninstall_hook(__FILE__, array('dalMachine', 'uninstallMachine'));



        // ajouter le menu de gestion des membres
        add_action('admin_menu', array($this, "add_menu_back"));
        add_action('admin_menu', array($this, "add_menu_backCoach"));
        add_action('admin_menu', array($this, "add_menu_backAqua"));
        add_action('admin_menu', array($this, "add_menu_backFitness"));
        add_action('admin_menu', array($this, "add_menu_backMachine"));

        // ajouter nos widgets
        add_action('widgets_init', function (){ register_widget('RegisterWidget'); });
        add_action('widgets_init', function (){ register_widget('RegisterAquaWidget'); });
        add_action('widgets_init', function (){ register_widget('RegisterFitnessWidget'); });
        add_action('widgets_init', function (){ register_widget('RegisterMachineWidget'); });

    }

// Menu back Membre
    
    public function add_menu_back()
    {
        $hook = add_menu_page("Les membres", "Membres",
                        "manage_options", "member", array($this, 'memberManager'),
                "dashicons-groups", 45);
        add_submenu_page('member', "Ajouter un membre", 'Ajouter',
        'manage_options','addMember', array($this, 'memberManager'));
    }

//Menu back Coach

    public function add_menu_backCoach()
    {
        $hook = add_menu_page("Les coachs", "Coach",
                        "manage_options", "coach", array($this, 'CoachManager'),
                "dashicons-groups", 45);
        add_submenu_page('coach', "Ajouter un coach", 'Ajouter',
        'manage_options','addCoach', array($this, 'CoachManager'));
    }

// Menu back Inscription Aqua

    public function add_menu_backAqua()
    {
        $hook = add_menu_page("Les inscription Aqua", "Aqua", "manage_options", "aqua", array($this, 'aquaManager'),
        "dashicons-clipboard", 45);

        add_submenu_page('aqua', "Ajouter un participant", 'Ajouter',
        'manage_options','addAqua', array($this, 'aquaManager'));
        
    }

// Menu back Inscription Fitness

    public function add_menu_backFitness()
        {
            $hook = add_menu_page("Les inscription Fitness", "Fitness", "manage_options", "fitness", array($this, 'fitManager'),
            "dashicons-clipboard", 45);

            add_submenu_page('fitness', "Ajouter un participant", 'Ajouter',
            'manage_options','addFitness', array($this, 'fitManager'));
            
        }
        
// Manu back Inscription Machine

    public function add_menu_backMachine()
            {
                $hook = add_menu_page("Les inscription Machine", "Machine", "manage_options", "machine", array($this, 'machineManager'),
                "dashicons-clipboard", 45);

                add_submenu_page('machine', "Ajouter un participant", 'Ajouter',
                'manage_options','addMachine', array($this, 'machineManager'));
                
            }

// Gestion des Membres

    public function memberManager()
    {
        $ins = new dal();
        echo '<div class="wrap">';
        echo "<h1>" . get_admin_page_title() ."</h1>";

        switch ( $_REQUEST['page'] ){
            case 'member':
                if( isset($_POST['nom']) && !empty($_POST['nom']) ){
                    $ins->saveMember();
                }

                $table = new ListTableMember();
                $table->prepare_items();
                echo "<form id='' method='get'>";
                echo '<input type="hidden" name="page" value="member"/>';
                echo $table->display();
                echo "</form>";

                break;
            case 'addMember':
                echo "ici un formulaire";
                echo "<form method='post'>";
                echo "<p>".
                    "<label for='nom'>Nom :</label>".
                    "<input name='nom' class='widefat' id='nom' value='' type='text'/>".
                    "</p>";
                echo "<p>".
                    "<label for='prenom'>Prenom :</label>".
                    "<input name='prenom' class='widefat' id='prenom' value='' type='text'/>".
                    "</p>";
                echo "<p>".
                    "<label for='mdp'>mot de passe :</label>".
                    "<input name='mdp' class='widefat' id='mdp' value='' 
                        type='text'/>".
                    "</p>";
                echo "<input type='hidden' name='page' value='member' />";
                echo "<p>".
                    "<label for='email'>Email :</label>".
                    "<input name='email' class='widefat' id='email' value='' type='email'/>".
                    "</p>";
                echo "<p><input type='submit' value='Enregistrer'></p>";
                echo "</form>";

                break;
        }
    echo "</div>";

    }


//Gestion des Coach

public function CoachManager()
    {
        $ins = new dalCoach();
        echo '<div class="wrap">';
        echo "<h1>" . get_admin_page_title() ."</h1>";

        switch ( $_REQUEST['page'] ){
            case 'coach':
                if( isset($_POST['nom']) && !empty($_POST['nom']) ){
                    $ins->saveCoach();
                }

                $table = new ListTableCoach();
                $table->prepare_items();
                echo "<form id='' method='get'>";
                echo '<input type="hidden" name="page" value="coach"/>';
                echo $table->display();
                echo "</form>";

                break;
            case 'addCoach':
                echo "ici un formulaire";
                echo "<form method='post'>";
                echo "<p>".
                    "<label for='nom'>Nom :</label>".
                    "<input name='nom' class='widefat' id='nom' value='' type='text'/>".
                    "</p>";
                echo "<p>".
                    "<label for='prenom'>Prenom :</label>".
                    "<input name='prenom' class='widefat' id='prenom' value='' type='text'/>".
                    "</p>";

                echo "<p>".
                    "<label for='discipline'>Discipline :</label>".
                    "<input name='discipline' class='widefat' id='discipline' value='' type='text'/>".
                    "</p>";

                echo "<input type='hidden' name='page' value='coach' />";
                echo "<p>".
                    "<label for='description'>Présentation :</label>".
                    "<input name='description' class='widefat' id='description' value='' type='text'/>".
                    "</p>";
                echo "<p><input type='submit' value='Enregistrer'></p>";
                echo "</form>";

                break;
        }
        echo "</div>";
    }

//gestion des inscriptions Aqua

public function aquaManager()
    {
        $ins = new dalAqua();
        echo '<div class="wrap">';
        echo "<h1>" . get_admin_page_title() ."</h1>";

        switch ( $_REQUEST['page'] ){
            case 'aqua':
                if( isset($_POST['nom']) && !empty($_POST['nom']) ){
                    $ins->saveAqua();
                }

                $table = new ListTableAqua();
                $table->prepare_items();
                echo "<form id='' method='get'>";
                echo '<input type="hidden" name="page" value="aqua"/>';
                echo $table->display();
                echo "</form>";

                break;

            case 'addAqua':
                $this->dalAqua = new dalAqua();
                $data = $this->dalAqua->findALlAqua();
                $c = count($data);
                if($c < 15){
                
                    echo "ici un formulaire";
                    echo "<form method='post'>";
                    echo "<p>".
                        "<label for='nom'>Nom :</label>".
                        "<input name='nom' class='widefat' id='nom' value='' type='text'/>".
                        "</p>";
                    echo "<p>".
                        "<label for='prenom'>Prenom :</label>".
                        "<input name='prenom' class='widefat' id='prenom' value='' type='text'/>".
                        "</p>";
                    echo "<input type='hidden' name='page' value='aqua' />";
                    echo "<p><input type='submit' value='Enregistrer'></p>";
                    echo "</form>";
                }
                else {
                    echo "Vous avez atteint la limite de membres pour cette catégorie";
                }

                break;
        }
        echo "</div>";
    }

// Gestion des Fitness

public function fitManager()
    {
        $ins = new dalFitness();
        echo '<div class="wrap">';
        echo "<h1>" . get_admin_page_title() ."</h1>";

        switch ( $_REQUEST['page'] ){
            case 'fitness':
                if( isset($_POST['nom']) && !empty($_POST['nom']) ){
                    $ins->saveFitness();
                }

                $table = new ListTableFitness();
                $table->prepare_items();
                echo "<form id='' method='get'>";
                echo '<input type="hidden" name="page" value="fitness"/>';
                echo $table->display();
                echo "</form>";

                break;

            case 'addFitness':
                $this->dalFitness = new dalFitness();
                $data = $this->dalFitness->findALlFitness();
                $c = count($data);
                if($c < 20){
                echo "ici un formulaire";
                echo "<form method='post'>";
                echo "<p>".
                    "<label for='nom'>Nom :</label>".
                    "<input name='nom' class='widefat' id='nom' value='' type='text'/>".
                    "</p>";
                echo "<p>".
                    "<label for='prenom'>Prenom :</label>".
                    "<input name='prenom' class='widefat' id='prenom' value='' type='text'/>".
                    "</p>";
                echo "<input type='hidden' name='page' value='fitness' />";
                echo "<p><input type='submit' value='Enregistrer'></p>";
                echo "</form>";
                }
                else{
                    echo "Vous avez atteint les limite de membres pour cette catégorie";
                }
                break;
        }
        echo "</div>";
    }

//Gestion des machines

public function machineManager()
    {
        $ins = new dalMachine();
        echo '<div class="wrap">';
        echo "<h1>" . get_admin_page_title() ."</h1>";

        switch ( $_REQUEST['page'] ){
            case 'machine':
                if( isset($_POST['nom']) && !empty($_POST['nom']) ){
                    $ins->saveMachine();
                }

                $table = new ListTableMachine();
                $table->prepare_items();
                echo "<form id='' method='get'>";
                echo '<input type="hidden" name="page" value="machine"/>';
                echo $table->display();
                echo "</form>";

                break;

            case 'addMachine':
                echo "ici un formulaire";
                echo "<form method='post'>";
                echo "<p>".
                    "<label for='nom'>Nom :</label>".
                    "<input name='nom' class='widefat' id='nom' value='' type='text'/>".
                    "</p>";
                echo "<p>".
                    "<label for='prenom'>Prenom :</label>".
                    "<input name='prenom' class='widefat' id='prenom' value='' type='text'/>".
                    "</p>";
                echo "<p>".
                    "<label for='dateM'>Date :</label>".
                    "<input name='dateM' class='widefat' id='dateM' value='' type='date'/>".
                    "</p>";
                echo "<p>".
                    "<label for='heure'>Heure :</label>".
                    "<input name='heure' class='widefat' id='heure' value='' type='text'/>".
                    "</p>";
                echo "<input type='hidden' name='page' value='machine' />";
                echo "<p><input type='submit' value='Enregistrer'></p>";
                echo "</form>";

                break;
        }
        echo "</div>";
    }


//Init des scripts

    function cfa_script()
    {
        // declaration de bootstrap
        wp_enqueue_style('bootstrap',
            "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css",
            array(),'1.0');
        wp_enqueue_style('main',
            plugins_url("/css/main.css", __FILE__),
            array(),'1.0');
        wp_enqueue_script('jqueryjs',
            "https://code.jquery.com/jquery-3.5.1.min.js",
            array(),'3.5.1',true
        );
        wp_enqueue_script('popper',
            "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.5.4/umd/popper.min.js",
            array('jqueryjs'),'2.5.4',true
        );
        wp_enqueue_script('btjs',
            "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js",
            array('popper'),'4.5.2',true
        );
        wp_enqueue_script('main',
            plugins_url("/js/main.js", __FILE__),
            array("btjs"), "1.0", true);
    }

    function cfa_script_admin(){
        wp_enqueue_style('bootstrap',
            "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css",
            array(),'1.0');
        wp_enqueue_script('main',
            plugins_url("/js/main.js", __FILE__),
            array("btjs"), "1.0", true);
    }

   
    public static function get_instance() {
       if( null == self::$instance) {
           self::$instance = new cfa();
       }
       return self::$instance;
    }

}


new cfa();
add_action('plugin_loader', array('cfa', 'get_instance'));
