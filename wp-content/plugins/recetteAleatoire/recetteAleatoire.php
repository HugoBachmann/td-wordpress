<?php
/**
 * Plugin Name: Recette aléatoire
 * Description: Ce plugin affiche des recettes fitness de manière aléatoire
 * Author: Hugo Bachmann
 */

function wp_custom_post_type() {

    
    $labels = array(
		
		'name'                => _x( 'Recette diététique', 'Post Type General Name'),
		
		'singular_name'       => _x( 'Recette diététique', 'Post Type Singular Name'),
		
		'menu_name'           => __( 'Recette diététique'),
	
		'all_items'           => __( 'Toutes les recettes diététiques'),
		'view_item'           => __( 'Voir les recettes diététiques'),
		'add_new_item'        => __( 'Ajouter une nouvelle recette diététique'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer la recette diététique'),
		'update_item'         => __( 'Modifier la recette diététique'),
		'search_items'        => __( 'Rechercher une recette diététique'),
		'not_found'           => __( 'Non trouvée'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);
	
	
	$args = array(
		'label'               => __( 'Recette diététique'),
		'description'         => __( 'Tous sur les recettes diététiques'),
		'labels'              => $labels,

        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),

		'show_in_rest' => true,
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug' => 'recette-dietetique'),

	);
	
	register_post_type( 'recettedietetique', $args );

}

add_action( 'init', 'wp_custom_post_type', 0 );

// fonction custom post type recette aléatoire apppeler en front par un shortcute dans un widget text

function wpb_rand_posts() { 
 
    $string = "";

    $args = array(
        'post_type' => 'recette dietetique',
        'orderby'   => 'rand',
        'posts_per_page' => 1, 
        );
     
    $the_query = new WP_Query( $args );
     
    if ( $the_query->have_posts() ) {
     
    $string .= '<div class="recette">';
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $string .= '<h3>' . get_the_title() . '</h3><p>' . get_the_content() . '</p>';
        }
		$string .= '</div>';
		
        wp_reset_postdata();
    } else {
     
    $string .= 'Aucune recette trouvée';
    }
     
    return $string; 
    } 
     
    add_shortcode('wpb-random-posts','wpb_rand_posts');
    add_filter('widget_text', 'do_shortcode'); 