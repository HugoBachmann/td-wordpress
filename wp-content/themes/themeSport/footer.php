 <?php wp_footer() ?>
    <footer>
        <div class="container">
        <div class="row">
            <div class="footerInfo">
                <div class="register">
                    <?= get_sidebar('footerWidget'); ?>
                </div>
                <div class="footertext">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora esse qui consequuntur hic aliquam nesciunt reiciendis neque molestiae dolorum, praesentium sapiente aperiam eligendi. Sunt voluptatibus a provident enim, dolorum nulla.</p>
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-lg-6" id="info_legale">
                    <a href="">Mentions légales</a>
                    <a href="">Conditions générales</a>
                </div>
                <div class="col-lg-6" id="realisation">
                    Réalisé par <a href="https://hugobachmann.com/" target="_blank">Hugo Bachmann</a>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>