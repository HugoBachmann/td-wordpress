<!DOCTYPE html>
<html lang="en">
<head>
    <title> <?php bloginfo( 'name' ); echo wp_title();?> </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,400;0,500;0,700;1,700&display=swap" rel="stylesheet"> 
    <?php wp_head() ?>
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <div id="logo" class="col-lg-3">
                    <figure>
                        <img src="<?php echo get_template_directory_uri();?>/img/logoAntiqueSport.png" alt="logo">
                    </figure>
                </div>
                <div id="main_menu" class="col-lg-9">
                    <?php wp_nav_menu(); ?>
                </div>
            </div>
        </div> 
    </header>