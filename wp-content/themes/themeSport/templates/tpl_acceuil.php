<?php

/**
 * Template Name: Accueil
 */
?>

<?php get_header() ?>
    
    
<section id="top_view">
<div class="container">  
    <div class="row top_container"> 

        <div id="container_title">
            <div id="title">
                <h1>Affuter <br><span class="title_part"> votre corps </span></h1>
            </div>
        </div>
        
        <figure id="img_top">
            <img src="<?php echo get_template_directory_uri();?>/img/muscleManMain.png" alt="homme musclé"> 
        </figure>

        <div id="slogan">
            <p>
                <?php if ( $slogan = get_field( 'slogan' ) ) : ?>
                    <?php echo $slogan; ?>
                <?php endif; ?>
            </p>
        </div>

        <div id="btn_plus">
            <a href="">En savoir plus</a>
        </div>

        </div>
    </div>
</section>
<section id="middle_view">
<div class="container">
    <div class="row" id="middle_container"> 
        <div id="title_middle">
            <?php if ( $titre_second = get_field( 'titre_second' ) ) : ?>
                <h2>LE SPORT NE FORGE PAS <br> &ensp;LE CARACTÈRE IL LE RÉVÈLE</h2>
            <?php endif; ?>
        </div>

        <div id="presentation">
                <p>
                    <?php if ( $paragraphe_second = get_field( 'paragraphe_second' ) ) : ?>
                        <?php echo $paragraphe_second; ?>
                    <?php endif; ?> 
                </p>
        </div>
        
        <figure id="img_middle">
            <img src="<?php echo get_template_directory_uri();?>/img/muscleWomanMiddle.png" alt="femme sportive">
        </figure>

        <div id="btn_plus_middle">
            <a href="">En savoir plus</a>
        </div>
    </div>
</div>
</section>
<section id="bottom_view">
    <div class="container">
        <div class="row" id="container_bottom">

            <div id="bottom_title">
                <h2>Le sport dans les meilleur conditions</h2>
            </div>

            <div id="team">
                <p>
                    <?php if ( $paragraphe_bottom = get_field( 'paragraphe_bottom' ) ) : ?>
                        <?php echo $paragraphe_bottom; ?>
                    <?php endif; ?>
                 </p>
             </div>

            <div id="img_presentation">
                
                <div id="img_left" class="col-lg-6">
                    <div id="img_1">
                        <?php
                        $image_1 = get_field( 'image_1' );
                        if ( $image_1 ) : ?>
                            <img src="<?php echo esc_url( $image_1['url'] ); ?>" alt="<?php echo esc_attr( $image_1['alt'] ); ?>" />
                        <?php endif; ?>
                        <div class="container_txt_img">
                            <p class="txt_img">
                                <?php if ( $text_image_1 = get_field( 'text_image_1' ) ) : ?>
                                    <?php echo $text_image_1; ?>
                                <?php endif; ?>
                            </p>
                        </div>
                    </div>

                    <div id="img_2">
                        <?php
                        $image__2 = get_field( 'image__2' );
                        if ( $image__2 ) : ?>
                            <img src="<?php echo esc_url( $image__2['url'] ); ?>" alt="<?php echo esc_attr( $image__2['alt'] ); ?>" />
                        <?php endif; ?>
                        <div class="container_txt_img">
                            <p class="txt_img">
                                <?php if ( $text_image_2 = get_field( 'text_image_2' ) ) : ?>
                                    <?php echo $text_image_2; ?>
                                <?php endif; ?>
                            </p>
                        </div>
                    </div>
                </div>

                <div id="img_right" class="col-lg-6">

                    <div id="img_3">
                        <?php
                        $image_3 = get_field( 'image_3' );
                        if ( $image_3 ) : ?>
                            <img src="<?php echo esc_url( $image_3['url'] ); ?>" alt="<?php echo esc_attr( $image_3['alt'] ); ?>" />
                        <?php endif; ?>
                        <div class="container_txt_img">
                            <p class="txt_img">
                                <?php if ( $text_image_3 = get_field( 'text_image_3' ) ) : ?>
                                    <?php echo $text_image_3; ?>
                                <?php endif; ?>
                            </p>
                        </div>
                    </div>

                    <div id="img_4">
                        <?php
                        $image_4 = get_field( 'image_4' );
                        if ( $image_4 ) : ?>
                            <img src="<?php echo esc_url( $image_4['url'] ); ?>" alt="<?php echo esc_attr( $image_4['alt'] ); ?>" />
                        <?php endif; ?>
                        <div class="container_txt_img">
                            <p class="txt_img">
                                <?php if ( $text_image_4 = get_field( 'text_image_4' ) ) : ?>
                                    <?php echo $text_image_4; ?>
                                <?php endif; ?>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section id="recette">
    <div class="container">
        <div class="row">
            <div id="recette_title">
                <h2>Retrouver toutes nos recettes diététique</h2>
            </div>
            <div class="main_content">
                <div class="col-lg-6">
                    <div class="col-lg-12 card randRecette">
                        <?= get_sidebar('homepage'); ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Numquam quidem esse totam asperiores iste tenetur sit perspiciatis eligendi in nam, explicabo, incidunt doloribus, voluptatibus debitis consequatur aspernatur illum eaque qui.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>



<?php get_footer() ?>