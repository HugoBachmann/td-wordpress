<?php

/**
 * Template Name: Machine
 */
?>

<?php get_header() ?>

<section id="machine_top_view">
    <div class="container">
        <div class="row top_container">

            <div id="machine_container_title">
                <div id="machine_title">
                    <h1>Sport <br><span class="machine_title_part"> sur machine </span></h1>
                </div>
            </div>

            <figure id="machine_img_top">
                <img src="<?php echo get_template_directory_uri();?>/img/sportMachine.jpg" alt="homme musclé"> 
            </figure>

            <div id="machine_presentation">
                <p>
                    <?php if ( $presentaion = get_field( 'presentaion' ) ) : ?>
                        <?php echo $presentaion; ?>
                    <?php endif; ?>
                </p>
             </div>

            <div id="machine_btn_plus">
                <a href="">En savoir plus</a>
            </div>

        </div>
    </div>
</section>



<section class="presentation_coach">
    <div class="container">
        <div class="row">
            <div class="coachs">
            <?php
                $result = $wpdb->get_results("SELECT * FROM wp_coach WHERE discipline = 'machine'");
                
                foreach ($result as $print) {
                    echo '
                    <div class="coach">
                    <div class="photo">
                        <figure>
                            <img src="'. get_template_directory_uri() .'/img/coach/'. $print->prenom .'.jpg" alt="coach">
                        </figure>
                    </div>
                    <div class="nom">
                        <p>' . $print->prenom .'</p>
                    </div>
                    <div class="desc_coach">
                        <p>'. $print->description .'</p>
                    </div>
                </div>

                    ';
                }
            ?>      
            </div>
        </div>
    </div>

</section>

<section id="machine_planing">
    <?php get_sidebar('machinewidget')?>
</section>

<?php get_footer() ?>
