<?php

/**
 * Template Name: Fitness
 */
?>

<?php get_header() ?>

<section id="fitness_top_view">
    <div class="container">
        <div class="row top_container">

            <div id="fitness_container_title">
                <div id="fitness_title">
                    <h1>Pratiquer <br><span class="fitness_title_part"> le fitness </span></h1>
                </div>
            </div>

            <figure id="fitness_img_top">
                <img src="<?php echo get_template_directory_uri();?>/img/fitness.jpg" alt="homme musclé"> 
            </figure>

            <div id="fitness_presentation">
                <p>
                    <?php if ( $presentation = get_field( 'presentation' ) ) : ?>
                        <?php echo $presentation; ?>
                    <?php endif; ?>
                </p>
             </div>

            <div id="fitness_btn_plus">
                <a href="">En savoir plus</a>
            </div>

        </div>
    </div>
</section>

<section class="presentation_coach">
    <div class="container">
        <div class="row">
            <div class="coachs">
            <?php
                $result = $wpdb->get_results("SELECT * FROM wp_coach WHERE discipline = 'fitness'");
                
                foreach ($result as $print) {
                    echo '
                    <div class="coach">
                    <div class="photo">
                        <figure>
                            <img src="'. get_template_directory_uri() .'/img/coach/'. $print->prenom .'.jpg" alt="coach">
                        </figure>
                    </div>
                    <div class="nom">
                        <p>' . $print->prenom .'</p>
                    </div>
                    <div class="desc_coach">
                        <p>'. $print->description .'</p>
                    </div>
                </div>

                    ';
                }
            ?>      
            </div>
        </div>
    </div>

</section>


<section id="fitness_planing">
            <div class="calendrier">
            <table class="table table-dark test">
                <thead>
                    <tr>
                    <th scope="col">Horaires</th>
                    <th scope="col">Lundi</th>
                    <th scope="col">Mardi</th>
                    <th scope="col">Mercredi</th>
                    <th scope="col">Jeudi</th>
                    <th scope="col">Vendredi</th>
                    <th scope="col">Samedi</th>
                    <th scope="col">Dimanche</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">9h00</th>
                        <td>Entrainement</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Entrainement</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                    <th scope="row">10h00</th>
                        <td></td>
                        <td></td>
                        <td>Entrainement</td>
                        <td></td>
                        <td>Entrainement</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                    <th scope="row">11h00</th>
                        <td>Entrainement</td>
                        <td></td>
                        <td></td>
                        <td>Entrainement</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                    <th scope="row">12h00</th>
                        <td></td>
                        <td></td>
                        <td>Entrainement</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                    <th scope="row">14h00</th>
                        <td></td>
                        <td>Entrainement</td>
                        <td></td>
                        <td>Entrainement</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                    <th scope="row">15h00</th>
                        <td>Entrainement</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Entrainement</td>
                        <td></td>
                    </tr>
                    <tr>
                    <th scope="row">16h00</th>
                        <td></td>
                        <td>Entrainement</td>
                        <td></td>
                        <td>Entrainement</td>
                        <td>Entrainement</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                    <th scope="row">17h00</th>
                        <td></td>
                        <td></td>
                        <td>Entrainement</td>
                        <td></td>
                        <td></td>
                        <td>Entrainement</td>
                        <td></td>
                    </tr>
                </tbody>
                </table>
            </div>
            <div class="widgetRegister">
                <?php get_sidebar('fitnesswidget')?>
            </div>
</section>

<?php get_footer() ?>
