<?php 
add_theme_support('title-tag');

function enregistre_mon_menu() {
    register_nav_menu( 'menu_principal', __( 'Menu principal' ) );
}
add_action( 'init', 'enregistre_mon_menu' );


function enregistre_mes_widget () {
    register_sidebar([
        'id' => 'aquawidget',
        'name' => 'aqua widget',
    ]);
    register_sidebar([
        'id' => 'fitnesswidget',
        'name' => 'fitness widget',
    ]);
    register_sidebar([
        'id' => 'machinewidget',
        'name' => 'machine widget',
    ]);
    register_sidebar ([
        'id' => 'homepage',
        'name' => 'widget recette aléatoire',
        ]);
    register_sidebar([
        'id' => 'footerWidget',
        'name' => 'footerWidget',
    ]);
    
    }

add_action( 'widgets_init', 'enregistre_mes_widget' );

?>

<?php
    // Chargement des styles et des scripts Bootstrap sur WordPress
    function wpbootstrap_styles_scripts(){
        wp_enqueue_style('style', get_stylesheet_uri());
        wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
        wp_enqueue_script('jquery');
        wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery'), 1, true);
        wp_enqueue_script('boostrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery', 'popper'), 1, true);
    }
    add_action('wp_enqueue_scripts', 'wpbootstrap_styles_scripts');
?>
